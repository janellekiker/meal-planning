# APIs

## Favorites Endpoints
![Favorites Endpoints](./images/saves.png)

### Get all favorite recipes

- Endpoint path: `/api/favorites`
- Endpoint method: `GET`

- Headers:

  - Authorization: Bearer token

- Response: A list of user's favorite recipes
- Response shape:
  ```json
  {
    [
      {
            "id": int,
            "label": string,
            "image": string,
            "source": datetime,
            "url": string,
            ***UNKNOWN***"href": string,
            "category_name": string,
            "user_id": int
      }
    ]
  }
  ```

### Create a favorite recipe

- Endpoint path: `/api/favorites`
- Endpoint method: `POST`

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
      {
            "label": string,
            "image": string,
            "source": datetime,
            "url": string,
            ***UNKNOWN***"href": string,
            "category_name": int,
      }
  ```

- Response: The recipe that was saved with added id and user_id properties
- Response shape:
  ```json
       {
            "id": int,
            "label": string,
            "image": string,
            "source": datetime,
            "url": string,
            ***UNKNOWN***"href": string,
            "category_name": string,
            "category_name": int,
            "user_id": int
       }
  ```

### Update a favorited recipe

- Endpoint path: `/api/favorites/{favorites_id}`
- Endpoint method: `PUT`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "category_name": string,
      }
  ```

- Response: An indication of success or failure
- Response shape:
  ```json
       {
           "success": true|false
       }
  ```

### Delete a favorited recipe

- Endpoint path: `/api/favorites/{favorite_id}`
- Endpoint method: `Delete`

- Headers:

  - Authorization: Bearer token

- Response: An indication of success or failure
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

## Categories Endpoints
![Category Endpoints](./images/categories.png)

### Get all categories

- Endpoint path: `/api/categories`
- Endpoint method: `GET`

- Headers:

  - Authorization: Bearer token

- Response: A list of user's created categories
- Response shape:
  ```json
  {
    [
      {
          "id": int,
          "category_name": string
      }
    ]
  }
  ```

### Create a category

- Endpoint path: `/api/categories`
- Endpoint method: `POST`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "category_name": string,
      }
  ```

- Response: The category that was saved with added id property
- Response shape:
  ```json
      {
          "id": int,
          "category_name": string
      }
  ```

### Update a category

- Endpoint path: `/api/categories/{category_id}`
- Endpoint method: `PUT`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "category_name": string,
      }
  ```

- Response: A list of user's saved claims
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

### Delete a category

- Endpoint path: `/api/categories/{category_id}`
- Endpoint method: `Delete`

- Headers:

  - Authorization: Bearer token

- Response: Dictionary
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

## Recipes Endpoints
![Recipes Endpoints](./images/claims.png)

### Get All Recipes from Third Party

- Endpoint path: `/api/recipes`
  -url: (f'https://api.edamam.com/api/recipes/v2?app_id={SECRET_API_ID}&app_key={SECRET_API_KEY}'))
- Endpoint method: `GET`

- Headers:

  - Authorization: SECRET API KEY, SECRET API ID
  - query: string(query)
  - type: string(public)

- Response: An array of recipes with correct information to match the input from the user
- Response shape:

  ```json
  {
    [
      {
          "query": string,
          "label": string,
          "image": string,
          "source": datetime,
          "url": string,
          ***UNKNOWN***"href": string,

      }
    ]
  }
  ```

### Get Recipe Details

- Endpoint path: `/api/recipes/{id}`
- Endpoint method: `GET`

- Headers:

  - Authorization: SECRET API KEY, SECRET API ID
  - id: string(id)
  - type: string(public)

- Response: Details for the recipe
- Response shape:
  ```json
  {
    [
      {
        "shareAs": string,
        "dietLabels": List[string],
        "healthLabels": List[string],
        "cautions": List[str],
        "ingredientLines": List[str],
        "ingredients": List[Ingredients],
        "href": str
      }
    ]
  }
  ```
