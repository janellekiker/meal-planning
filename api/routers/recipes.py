from fastapi import APIRouter, Depends

router = APIRouter()


# FAVORITE A RECIPE
@router.post("/api/favorites")
def create_favorite():
    pass

# SEE ALL FAVORITED RECIPES
@router.get("/api/favorites")
def get_all_favorites():
    pass

# UPDATE FAVORITED RECIPE
@router.put("/api/favorites/{favorite_id}")
def update_favorite(
    favorite_id: str
):
    pass

# DELETE A FAVORITE RECIPE
@router.delete("/api/favorites/{favorite_id}")
def delete_favorite(
    favorite_id: str
):
    pass
