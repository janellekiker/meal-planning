from fastapi import APIRouter, Depends
from queries.recipes import RecipeQueries

router = APIRouter()

# GET ALL RECIPES FROM THIRD PARTY
@router.get('/api/recipes')
def get_all_recipes(
    q: str,
    type: str = "public",
    repo: RecipeQueries = Depends()
):
    return {
        'recipes': repo.get_all(q, type)
    }

# RECIPE DETAIL BY ID
@router.get('/api/recipes/{id}')
def get_recipe_by_id(
    id: str,
    type: str = "public",
    repo: RecipeQueries = Depends()
):
    return repo.get_one_by_id(id, type)
