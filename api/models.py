from pydantic import BaseModel
from typing import List


class LinkSelf(BaseModel):
    href: str
    title: str


class Link(BaseModel):
    self: LinkSelf


class RecipeId(BaseModel):
    label: str
    image: str
    source: str
    url: str
    _links: Link


class Recipe(BaseModel):
    recipe: RecipeId


class RecipeIdList(BaseModel):
    hits: List[Recipe]


class Ingredients(BaseModel):
    text: str
    quantity: int
    measure: str
    food: str
    weight: int
    foodCategory: str
    foodId: str
    image: str


class RecipeDetails(RecipeId):
    shareAs: str
    dietLabels: List[str]
    healthLabels: List[str]
    cautions: List[str]
    ingredientLines: List[str]
    ingredients: List[Ingredients]
    href: str
