import requests
import os

SECRET_API_KEY = os.environ["SECRET_API_KEY"]
SECRET_API_ID = os.environ["SECRET_API_ID"]


class RecipeQueries:
    def get_all(self, q: str, type: str):
        params = {'q': q, 'type': type}
        res = requests.get(
            (f'https://api.edamam.com/api/recipes/v2?app_id={SECRET_API_ID}&app_key={SECRET_API_KEY}'),
            params=params)
        data = res.json()
        return data

    def get_one_by_id(self, id: str, type: str):
        params = {'type': type}
        url = (f'https://api.edamam.com/api/recipes/v2/{id}?app_id={SECRET_API_ID}&app_key={SECRET_API_KEY}')
        res = requests.get(url, params=params)
        data = res.json()
        return data
