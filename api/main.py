from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import thirdPartyRecipes, recipes

app = FastAPI()

app.include_router(thirdPartyRecipes.router, tags=['Third Party Recipes'])
app.include_router(recipes.router, tags=['Favorite Recipes'])


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
